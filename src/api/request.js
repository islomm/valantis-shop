import axios from "axios";
import md5 from "md5";

const apiUrl = "https://api.valantis.store:41000"; //  URL API
const password = "Valantis"; // Пароль для доступа к API

// Функция для формирования значения X-Auth в соответствии с указанным шаблоном
const generateXAuthHeaderValue = (password) => {
  const timestamp = new Date().toISOString().split("T")[0].replace(/-/g, "");
  const authString = `${password}_${timestamp}`;
  return md5(authString);
};

// Функция для отправки запроса к API
export const sendRequestToAPI = async (action, params = {}) => {
  const xAuthHeader = generateXAuthHeaderValue(password);
  const requestData = { action, params };

  try {
    const response = await axios.post(apiUrl, requestData, {
      headers: { "X-Auth": xAuthHeader },
    });

    if (response.status === 200) {
      return response.data.result;
    } else {
      console.error("Ошибка при отправке запроса:", response.statusText);
      return null;
    }
  } catch (error) {
    if (error.response) {
      console.error("Ошибка при отправке запроса:", error.response.statusText);
    } else {
      console.error("Ошибка при отправке запроса:", error.message);
    }
    return null;
  }
};

export function findUnRepeatedIds(arr) {
  const res = [];

  for (const item of arr) {
    if (!res.includes(item)) {
      res.push(item);
    }
  }
  // console.log("find " + res.length);

  return res;
}
