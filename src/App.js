import { useEffect, useState } from "react";
import { findUnRepeatedIds, sendRequestToAPI } from "../src/api/request";
import OneProduct from "./component/OneProduct";
import SkeletonLoader from "./component/SkeletonLoader";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Pagination from "./component/Pagination";

function App() {
  const initialState = JSON.parse(localStorage.getItem("datas")) || [];
  const [items, setItems] = useState(initialState);
  const [copiedItems, setCopiedItems] = useState();
  const savedBrands = JSON.parse(localStorage.getItem("datas")) || [];
  const [allBrands, setAllBrands] = useState(savedBrands);
  const [ids, setIds] = useState();
  const [nameFilter, setNameFilter] = useState("");
  const [priceFilter, setPriceFilter] = useState();
  const [brandFilter, setBrandFilter] = useState("");
  const [loader, setLoader] = useState(true);
  const [noProducts, setNoProducts] = useState(false);
  // pagination
  const [currentPage, setCurrentPage] = useState(1);
  const recordsPerPage = 50;
  const lastIndex = currentPage * recordsPerPage;
  const firstIndex = lastIndex - recordsPerPage;
  const products = items.slice(firstIndex, lastIndex);

  const npage = Math.ceil(items?.length / recordsPerPage);
  const numbers = [...Array(npage + 1).keys()]?.slice(1);
  const findBrands = (brand) => {
    try {
      const clickedBrandItems = copiedItems.filter(
        (item) => item.brand === brand
      );
      setItems(clickedBrandItems);
    } catch (err) {
      console.log("Error: " + err.message);
    }
  };

  useEffect(() => {
    try {
      sendRequestToAPI("get_ids", { offset: 0, limit: 101 }).then((result) => {
        if (result) {
          // console.log(result);
          sendRequestToAPI("get_items", {
            ids: findUnRepeatedIds(result),
          }).then((result) => {
            if (result) {
              const uniqueItems = {};

              const filteredItems = result.filter((item) => {
                if (!uniqueItems[item.id]) {
                  uniqueItems[item.id] = true;
                  return true;
                }
                return false;
              });
              const uniqueBrands = {};
              const filteredBrands = result.filter((item) => {
                if (!uniqueBrands[item.brand]) {
                  uniqueBrands[item.brand] = true;
                  return true;
                }
                return false;
              });
              localStorage.setItem("datas", JSON.stringify(filteredItems));
              localStorage.setItem("brands", JSON.stringify(filteredBrands));

              setItems(filteredItems);
              setAllBrands(filteredBrands);
              setCopiedItems(filteredItems);
              setLoader(false);
            } else {
              console.log("Запрос не выполнен.");
            }
          });
          setIds(findUnRepeatedIds(result));
        } else {
          console.log("Запрос не выполнен.");
        }
      });
    } catch (err) {
      console.log(`Error : ${err.message}`);
    }
  }, []);
  const PriceFilter = (e) => {
    e.preventDefault();
    setLoader(true);
    if (priceFilter) {
      try {
        const filteredPrice = [];
        copiedItems.filter((item) => {
          if (item.price == priceFilter) {
            filteredPrice.push(item);
          }
        });
        if (filteredPrice.length == 0) {
          setLoader(false);
          setNoProducts(true);
        } else {
          setItems(filteredPrice);
          toast.success("Successfully filtered !", {
            position: "top-right",
          });
          setNoProducts(false);
          setLoader(false);
          filteredPrice = [];
        }
      } catch (err) {
        console.log("Ошибка при фильтрации : " + err.message);
      }
    } else {
      toast.warning("Fill inputs !", {
        position: "top-right",
      });
      setLoader(false);
    }
  };
  const NameFilter = (e) => {
    e.preventDefault();
    setLoader(true);
    if (nameFilter) {
      try {
        const filteredName = [];
        copiedItems.filter((item) => {
          if (
            item?.product?.toLowerCase().includes(nameFilter?.toLowerCase())
          ) {
            filteredName.push(item);
          }
        });
        if (filteredName.length == 0) {
          setLoader(false);
          setNoProducts(true);
        } else {
          setItems(filteredName);
          setNoProducts(false);
          setLoader(false);
          filteredName = [];
        }
      } catch (err) {
        console.log("Ошибка при фильтрации : " + err.message);
      }
    } else {
      toast.warning("Fill inputs !", {
        position: "top-right",
      });
      setLoader(false);
    }
  };
  const BrandFilter = (e) => {
    e.preventDefault();
    setLoader(true);
    if (brandFilter) {
      try {
        const filteredBrand = [];
        copiedItems.filter((item) => {
          if (item?.brand?.toLowerCase().includes(brandFilter?.toLowerCase())) {
            filteredBrand.push(item);
          }
        });
        if (filteredBrand.length == 0) {
          setNoProducts(true);
          setLoader(false);
        } else {
          setItems(filteredBrand);
          setLoader(false);
          setNoProducts(false);
          filteredBrand = 1;
        }
      } catch (err) {
        console.log("Ошибка при фильтрации : " + err.message);
      }
    } else {
      toast.warning("Fill inputs !", {
        position: "top-right",
      });
      setLoader(false);
    }
  };
  const ClearFilter = (e) => {
    e.preventDefault();
    setLoader(false);
    try {
      setItems(copiedItems);
      setNoProducts(false);
      setLoader(false);
      setNameFilter("");
      setPriceFilter("");
      setBrandFilter("");
      toast.success("Successfully changed !", {
        position: "top-right",
      });
    } catch (error) {
      console.log(`Error: ${error.message}`);
    }
  };
  const prePage = () => {
    if (currentPage !== 1) {
      setCurrentPage(currentPage - 1);
    }
  };
  const changePage = (id) => {
    setCurrentPage(id);
  };
  const nextPage = () => {
    if (currentPage !== npage) {
      setCurrentPage(currentPage + 1);
    }
  };

  return (
    <div className="bg-slate-500 min-h-screen">
      <ToastContainer />
      <div className="flex justify-between border p-4 shadow-xl px-4 sm:px-8 bg-white items-center">
        <p className="text-center font-bold text-2xl text-red-600">LOGO</p>
        {allBrands &&
          copiedItems
            ?.filter(
              (item1) =>
                item1.brand !== null ??
                allBrands.some((item2) => item2.brand === item1.brand)
            )
            .map((item1) => (
              <p
                onClick={() => findBrands(item1.brand)}
                className="font-semibold cursor-pointer hover:text-red-600 text-md md:text-lg"
                key={item1.id}
              >
                {item1.brand}
              </p>
            ))}

        <p className="text-center font-bold text-2xl">USER</p>
      </div>
      <div className="flex ps-[20px] md:ps-[50px] md:gap-[450px] gap-12 items-center">
        <p className="font-bold text-3xl md:text-[40px] mt-2 text-white">
          Total : {items.length}
        </p>
        <Pagination
          currentPage={currentPage}
          prePage={prePage}
          changePage={changePage}
          nextPage={nextPage}
          numbers={numbers}
        />
      </div>

      <div className="flex sm:flex-col md:flex-row w-full">
        <div className="w-[70%] sm:w-[15%] border md:p-5 px-2 bg-slate-400 mt-11">
          <p className="text-white font-bold text-[40px] sm:text-center">
            Filter:
          </p>
          <form className="pt-[20px]" onSubmit={PriceFilter}>
            <input
              className="p-1 rounded-md w-full"
              placeholder="введите цену ..."
              value={priceFilter}
              onChange={(e) => setPriceFilter(e.target.value)}
            />
            <button
              onClick={PriceFilter}
              className="px-3 py-2 bg-red-600 text-white text-bold rounded-lg mt-5 w-full hover:border-red-600 hover:bg-gray-100 hover:text-red-600 ease-out duration-[300ms]"
            >
              Применить цену
            </button>
          </form>
          <form className="pt-[20px]" onSubmit={NameFilter}>
            <input
              className="p-1 rounded-md w-full"
              placeholder="введите продукта ..."
              value={nameFilter}
              onChange={(e) => setNameFilter(e.target.value)}
            />
            <button
              onClick={NameFilter}
              className="px-3 py-2 bg-red-600 text-white text-bold rounded-lg mt-5 w-full hover:border-red-600 hover:bg-gray-100 hover:text-red-600 ease-out duration-[300ms]"
            >
              Применить продукт
            </button>
          </form>
          <form className="pt-[20px]" onSubmit={BrandFilter}>
            <input
              className="p-1 rounded-md w-full"
              placeholder="введите бренд ..."
              value={brandFilter}
              onChange={(e) => setBrandFilter(e.target.value)}
            />
            <button
              onClick={BrandFilter}
              className="px-3 py-2 bg-red-600 text-white text-bold  rounded-lg font-semibold mt-5 w-full hover:border-red-600 hover:bg-gray-100 hover:text-red-600 ease-out duration-[300ms]"
            >
              Применить бренд
            </button>
          </form>
          <form className="pt-[50px]">
            <button
              onClick={ClearFilter}
              type="submit"
              className="bg-black text-white w-full py-2 font-bold rounded-lg hover:bg-gray-300 hover:text-black hover:border-2 hover:border-black ease-out duration-200"
            >
              Очистить фильтр
            </button>
          </form>
        </div>
        {loader ? (
          <SkeletonLoader />
        ) : noProducts ? (
          <p className="font-bold text-white text-center text-[40px] mt-5 py-[10%] mx-auto sm:mt-0">
            OOOOps No Products with your filter! 😔
          </p>
        ) : (
          <div className="w-full sm:w-[85%] flex flex-wrap px-4 sm:px-8 gap-4 sm:gap-12 py-12 bg-slate-500">
            <OneProduct products={products} />
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
