import React from "react";

const Pagination = ({
  prePage,
  nextPage,
  changePage,
  numbers,
  currentPage,
}) => {
  return (
    <>
      <div className="pb-1 justify-center flex mt-5">
        <ul className="flex">
          <li className="page-item">
            <a
              href="#"
              onClick={prePage}
              className="page-link bg-red-600 px-3 py-1 text-white font-semibold rounded-l"
            >
              prev
            </a>
          </li>

          {numbers?.map((item, id) => (
            <li key={id} className="cursor-pointer">
              <a
                onClick={() => changePage(item)}
                className={`px-2 py-1 ${
                  currentPage !== item
                    ? "bg-red-600 text-white border-white border-2"
                    : "border-2 border-red-600 bg-white"
                }`}
              >
                {item}
              </a>
            </li>
          ))}

          <li className="page-item">
            <a
              href="#"
              onClick={nextPage}
              className="page-link bg-red-600 px-3 py-1 text-white font-semibold rounded-r"
            >
              next
            </a>
          </li>
        </ul>
      </div>
    </>
  );
};

export default Pagination;
