import React from "react";
import SkeletonLoader from "./SkeletonLoader";

const OneProduct = ({ products }) => {
  return (
    <>
      {products ? (
        products?.map((item, index) => {
          return (
            <div
              key={index}
              className="border px-5 py-3 md:w-1/3 sm:w-1/2 w-full lg:w-1/5 xl:w-1/6  h-[333px] bg-gray-200 rounded-2xl hover:bg-slate-300 hover:shadow-2xl"
            >
              <p className="text-center bg-red-600 text-black font-bold text-white text-sm p-1 rounded-lg">
                ID:{item.id}
              </p>
              {item?.brand ? (
                <p className="py-2">
                  Brand: <span className="font-bold">{item?.brand}</span>
                </p>
              ) : (
                <p className="py-2">
                  Brand: <span className="font-bold">Unknown</span>
                </p>
              )}
              <div className="h-[100px]">
                <p className="pt-3 font-semibold">{item?.product}</p>
              </div>
              <p className="py-2">
                Price: $
                <span className="text-green-600 font-bold">{item?.price}</span>{" "}
              </p>
              {/* <button className="bg-red-500 px-3 py-1 text-white font-bold items-center rounded-md hover:border-red-600 hover:bg-gray-100 hover:text-red-600 ease-out duration-[300ms] hover:border-2 hover:border-red-600">
                Buy Now
              </button> */}
            </div>
          );
        })
      ) : (
        <SkeletonLoader />
      )}
    </>
  );
};

export default OneProduct;
