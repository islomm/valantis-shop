import React, { version } from "react";

const SkeletonLoader = () => {
  return (
    <div className="w-[85%] flex flex-wrap px-[25px] gap-12 py-12 bg-slate-500">
      <div className="border px-5 py-3 md:w-1/3 sm:w-1/2 w-full lg:w-1/5 xl:w-1/6 h-[333px] bg-gray-400 rounded-2xl animate-background">
        <div className="border px-5 py-3 w-full h-[50px] bg-gray-400 rounded-md animate-background"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background my-20"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background"></div>
      </div>
      <div className="border px-5 py-3 md:w-1/3 sm:w-1/2 w-full lg:w-1/5 xl:w-1/6 h-[333px] bg-gray-400 rounded-2xl animate-background">
        <div className="border px-5 py-3 w-full h-[50px] bg-gray-400 rounded-md animate-background"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-bDackground my-20"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background"></div>
      </div>
      <div className="border px-5 py-3 md:w-1/3 sm:w-1/2 w-full lg:w-1/5 xl:w-1/6 h-[333px] bg-gray-400 rounded-2xl animate-background">
        <div className="border px-5 py-3 w-full h-[50px] bg-gray-400 rounded-md animate-background"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background my-20"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background"></div>
      </div>
      <div className="border px-5 py-3 md:w-1/3 sm:w-1/2 w-full lg:w-1/5 xl:w-1/6 h-[333px] bg-gray-400 rounded-2xl animate-background">
        <div className="border px-5 py-3 w-full h-[50px] bg-gray-400 rounded-md animate-background"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background my-20"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background"></div>
      </div>
      <div className="border px-5 py-3 md:w-1/3 sm:w-1/2 w-full lg:w-1/5 xl:w-1/6 h-[333px] bg-gray-400 rounded-2xl animate-background">
        <div className="border px-5 py-3 w-full h-[50px] bg-gray-400 rounded-md animate-background"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background my-20"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background"></div>
      </div>
      <div className="border px-5 py-3 wmd:w-1/3 sm:w-1/2 w-full lg:w-1/5 xl:w-1/6 h-[333px] bg-gray-400 rounded-2xl animate-background">
        <div className="border px-5 py-3 w-full h-[50px] bg-gray-400 rounded-md animate-background"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background my-20"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background"></div>
      </div>
      <div className="border px-5 py-3 w-md:w-1/3 sm:w-1/2 w-full lg:w-1/5 xl:w-1/6 h-[333px] bg-gray-400 rounded-2xl animate-background">
        <div className="border px-5 py-3 w-full h-[50px] bg-gray-400 rounded-md animate-background"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background my-20"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background"></div>
      </div>
      <div className="border px-5 py-3 w-md:w-1/3 sm:w-1/2 w-full lg:w-1/5 xl:w-1/6 h-[333px] bg-gray-400 rounded-2xl animate-background">
        <div className="border px-5 py-3 w-full h-[50px] bg-gray-400 rounded-md animate-background"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background my-20"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background"></div>
      </div>
      <div className="border px-5 py-3 wmd:w-1/3 sm:w-1/2 w-full lg:w-1/5 xl:w-1/6 h-[333px] bg-gray-400 rounded-2xl animate-background">
        {" "}
        <div className="border px-5 py-3 w-full h-[50px] bg-gray-400 rounded-md animate-background"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background my-20"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background"></div>
      </div>
      <div className="border px-5 py-3 w-md:w-1/3 sm:w-1/2 w-full lg:w-1/5 xl:w-1/6 h-[333px] bg-gray-400 rounded-2xl animate-background">
        <div className="border px-5 py-3 w-full h-[50px] bg-gray-400 rounded-md animate-background"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background my-20"></div>
        <div className="border px-5 py-3 w-full h-[25px] bg-gray-400 rounded-md animate-background"></div>
      </div>
    </div>
  );
};

export default SkeletonLoader;
